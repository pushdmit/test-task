<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="body-content">

        <div class="row">

            <?php foreach($users as $user): ?>
                <div class="col-lg-4">
                    <h2><a href="/id<?=$user->id?>"><?=$user->username?></a></h2>
                </div>
            <?php endforeach; ?>

        </div>

    </div>
</div>
