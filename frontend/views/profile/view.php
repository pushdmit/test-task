<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use budyaga\cropper\Widget;
use budyaga\users\models\User;
use budyaga\users\components\AuthKeysManager;
use budyaga\users\UsersAsset;

/* @var $this yii\web\View */
/* @var $model \common\models\User */
/* @var $walls \common\models\Wall[] */

$this->title = Yii::t('users', 'PROFILE');
//$this->params['breadcrumbs'][] = $this->title;
//UsersAsset::register($this);
?>
<div class="site-profile">
    <div class="row">
        <div class="col-xs-12 col-md-2 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><?=$model->getNameFull()?></div>
                <div class="panel-body">
                    <img alt="140x140" class="img-thumbnail" data-src="holder.js/140x140" style="width: 140px; height: 140px;" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTQwIiBoZWlnaHQ9IjE0MCIgdmlld0JveD0iMCAwIDE0MCAxNDAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzE0MHgxNDAKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTAxZDk0ZjIzNSB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1MDFkOTRmMjM1Ij48cmVjdCB3aWR0aD0iMTQwIiBoZWlnaHQ9IjE0MCIgZmlsbD0iI0VFRUVFRSIvPjxnPjx0ZXh0IHg9IjQzLjUiIHk9Ijc0LjgiPjE0MHgxNDA8L3RleHQ+PC9nPjwvZz48L3N2Zz4=" data-holder-rendered="true">
                    <hr>
                    <p class="text-primary"><a href="/profile"><?=Yii::t('users', 'UPDATE');?></a></p>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading"><?=Yii::t('users', 'WALL');?></div>
                <div class="panel-body">
                    <div class="widget-content nopadding">
                        <ul id="wall" class="recent-posts" style="list-style-type: none">
                            <? foreach($walls as $item): ?>
                            <li>
                                <div class="user-thumb" style="float: left; padding-right: 10px">
                                    <img width="40" height="40" src="/images/avatar2.png" alt="User">
                                </div>
                                <div class="article-post">
                                    <span class="user-info">
                                        <a href="/id<?=$item->userIdAdd->id?>"><?=$item->userIdAdd->username ?></a>
                                        <?=$item->created_at ?>
                                    </span>
                                    <p><?=$item->text ?></p>
                                    <?php $form = ActiveForm::begin(['id' => 'form-profile', 'action'=>'/api/wall/delete']); ?>
                                    <?= Html::hiddenInput('id', $item->id, ['class' => 'form-control']) ?>
                                    <a class="btn btn-danger btn-xs send-form"><i class="glyphicon glyphicon-remove"></i></a>
                                    <?php ActiveForm::end(); ?>
                                </div>
                            </li>
                            <? endforeach; ?>
                            <li id="wall-item" style="display: none">
                                <div class="user-thumb" style="float: left; padding-right: 10px">
                                    <img class="photo" width="40" height="40" src="/images/avatar2.png" alt="User">
                                </div>
                                <div class="article-post">
                                    <span class="user-info">
                                        <a class="username" href=""></a>
                                        <span class="data"></span>
                                    </span>
                                    <p class="text"></p>
                                    <?php $form = ActiveForm::begin(['id' => 'form-profile', 'action'=>'/api/wall/delete']); ?>
                                    <?= Html::hiddenInput('id', '', ['class' => 'form-control id']) ?>
                                    <a class="btn btn-danger btn-xs send-form"><i class="glyphicon glyphicon-remove"></i></a>
                                    <?php ActiveForm::end(); ?>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <hr>
                    <?php $form = ActiveForm::begin(['id' => 'form-profile', 'action'=>'/api/wall/create']); ?>
                    <?= Html::hiddenInput('user_id', $model->getId(), ['class' => 'form-control']) ?>
                    <div class="form-group">
                        <?= Html::textarea('text', '', ['class' => 'form-control']) ?>
                    </div>
                    <div class="form-group" style="">
                        <?= Html::submitButton(Yii::t('users', 'ADD'), ['class' => 'btn btn-primary send-form', 'name' => 'profile-button']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
