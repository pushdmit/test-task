/**
 * Created by PUSHDMIT
 */

jQuery(function () {
    $('.reset-form').click(function (e) {
        var form = $(e.currentTarget).parents('form');
        form.find(':input:reset');
    });

    addEvent();

    function addEvent(){
        $('.send-form').unbind();

        $('.send-form').click(function (e) {
            var current = $(e.currentTarget);
            var form = current.parents('form');
            var data = getData(form);

            var popup = $('#progress-popup');
            cleanErrorForm(form);

            $.ajax({
                type: form.attr('method'),
                url: form.attr('action'),
                data: data,
                success: function (response) {
                    if (response.status == 200) {
                        if('wall' == response.type){
                            addWall(response.result);
                        } else if ('wall-delete' == response.type){
                            current.parents('li').remove();
                        } else{
                            location.href = current.attr('data-url') || location.href;
                        }
                    } else {
                        setErrorForm(form, response.errors);
                    }
                },
                error: function (response) {
                }
            });

            return false;
        });
    }


    function getData(form) {
        if (!form) return {};

        return form.serialize();

        //var data = {};
        //
        //form.find(':input').each(function () {
        //    if ($(this).attr('type') == 'checkbox') {
        //        if ($(this).is(':checked')) {
        //            data[$(this).attr('name')] = 'on';
        //        }
        //    } else if ($(this).attr('type') == 'radio') {
        //        if ($(this).is(':checked')) {
        //            data[$(this).attr('name')] = $(this).val();
        //        }
        //    } else {
        //        data[$(this).attr('name')] = $(this).val();
        //    }
        //});

        //return data;
    }

    function setErrorForm(form, errors) {

        console.log(errors);
        cleanErrorForm(form);

        for (var i in errors) {

            var input = form.find(':input[name=' + i + ']');
            input.parent().addClass('error');

            var div = input.parent();

            if (errors[i] instanceof Array) {
                for (var j in errors[i]) {
                    $('<p></p>', {
                        class: 'help-block help-block-error',
                        text: errors[i][j]
                    }).appendTo(div);
                }
            } else {
                $('<p></p>', {class: 'help-block help-block-error', text: errors[i]}).appendTo(div);
            }

        }
    }

    function addWall(data) {
        //console.log(data);
        var li = $('#wall-item').clone();
        li.find('.text').text(data.text);
        li.find('.username').text(data.username).attr('href','id'+data.user_id_add);
        li.find('.data').text(data.create_at);
        li.find('.id').val(data.id);
        li.attr('id','').css('display','').appendTo($('#wall'));
        addEvent();
    }

    function cleanErrorForm(form) {
        form.find('div.error').removeClass('error');
        form.find('p.help-block-error').remove();
        return 1;
    }
});
