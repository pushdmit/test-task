<?php

namespace frontend\controllers\api;

use common\models\Wall;
use Yii;
use yii\filters\AccessControl;

/**
 * Класс апи для работы с вопросами
 *
 * Class QuestionController
 * @package frontend\controllers\api
 */
class WallController extends ApiController
{
    protected $result = ['status'=>200, 'result'=>'Класс апи для работы со стеной'];

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'delete'],
                'rules' => [
                    [
                        'actions' => ['create', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex(){
        return $this->result;
    }

    /**
     * Добавление записи
     *
     * @return bool
     */
    public function actionCreate(){

        $model = new Wall();

        if ($model->load($this->get(), '')) {
            $model->user_id_add = Yii::$app->user->id;
            if($model->save()){
                $result = $model->toArray();
                $result['username'] = Yii::$app->user->identity->username;
                $result['avtar'] = '/images/avatar2.png';

                $this->result = ['status'=>200, 'type'=>'wall', 'result'=>$result];//
                return true;
            }

        }

        $this->result = ['status'=>600, 'errors'=>$model->errors];
        return false;
    }



    /**
     * Удаление записи
     *
     * @return bool
     */
    public function actionDelete(){
        $isDelete = Wall::deleteAll(
            'id = :id AND (user_id = :user_id OR user_id_add = :user_id)'
            , [':id' => $this->get('id'), ':user_id' => Yii::$app->user->id]
        );

        if ($isDelete) {
            $this->result = ['status'=>200, 'type'=>'wall-delete', 'result'=>$isDelete];
            return true;
        }

        $this->result = ['status'=>600, 'errors'=>$isDelete];
        return false;
    }

}