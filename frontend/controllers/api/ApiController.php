<?php

namespace frontend\controllers\api;

use yii\web\Controller;

class ApiController extends Controller
{
    protected $result = ['status'=>200, 'result'=>'test api'];

    public function afterAction($action, $result)
    {
        header('Content-Type: application/json');
        echo json_encode($this->result); die;
    }

    public function actionIndex(){
        return $this->result;
    }

    public function get($key = null, $default = null){
        if($key){
            return \Yii::$app->request->post($key, $default);
        }

        return \Yii::$app->request->post();
    }

    /**
     * @param $params string|array
     * @return array|bool
     */
    public function check_error($params){
        $errors = [];

        if(is_array($params)){
            $array = $this->get();
            foreach($params as $val){
                if(is_string($val)){
                    if(!array_key_exists($val, $array)){
                        $errors[$val] = 'Не передан обязательный аргумент '.$val.'.';
                    }
                }
            }
        } else {
            if(is_string($params)){
                if(!array_key_exists($params, $this->get())){
                    $errors[$params] = 'Не передан обязательный аргумент '.$params.'.';
                }
            }
        }

        if($errors){
            return $errors;
        }

        return false;
    }

}