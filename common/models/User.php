<?php
namespace common\models;

use Yii;

/**
 * User model
 *
 */
class User extends \budyaga\users\models\User
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 2;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
        ];
    }

    public function getNameFull(){
        return $this->username;
    }
}
